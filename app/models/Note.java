package models;

import org.joda.time.DateTime;
import play.data.format.Formats;
import play.db.ebean.Model;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.Date;

@Entity
public class Note extends Model {

    @Id
    public String id;

    public String title;

    public String content;

    @Formats.DateTime(pattern = "dd/MM/yyyy")
    public Date date;

}
