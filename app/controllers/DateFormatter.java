package controllers;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.Date;
import java.util.Locale;

public class DateFormatter {

    public static String formatDate(Date date) {

        return DateTimeFormat.forPattern("EEEEE, dd.MM.yyyy").withLocale(Locale.forLanguageTag("PL")).print(new DateTime(date.getTime()));

    }

}
