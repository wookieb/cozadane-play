package controllers;

import com.google.common.base.Joiner;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import models.Note;
import org.joda.time.DateTime;
import play.Logger;
import play.db.ebean.Model;

import java.util.Date;
import java.util.List;
import java.util.Set;

public class NoteService {

    public static List<Date> getNewDates() {
        List<Note> notes = getNotes();
        Set<Date> dates = Sets.newLinkedHashSet();

        for (Note note : notes) {
            dates.add(note.date);
        }
        Logger.info(Joiner.on(", ").join(dates));
        return Lists.newArrayList(dates);
    }

    public static List<Note> getNotes() {
        return new Model.Finder(String.class, Note.class)
                    //.where().ge("date", DateTime.now()) Uncomment to get only future notes
                    .order("date asc").findList();
    }

    public static List<Note> getNotes(Date date) {
        return new Model.Finder(String.class, Note.class)
                .where().eq("date", date)
                .findList();
    }

    public static Note getNote(String id) {
        return (Note) new Model.Finder(String.class, Note.class).byId(id);
    }

}
