package controllers;

import models.Note;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.form;
import views.html.index;
import views.html.last;
import views.html.note;

import java.util.Map;

public class Application extends Controller {

    public static Result index() {
        return ok(index.render(last.render(NoteService.getNotes())));
    }

    public static Result addNew() { return ok(index.render(form.render())); }

    public static Result showNote(String id) {
        return ok(index.render(note.render(NoteService.getNote(id))));
    }

    public static Result addNewFormClicked() {
        Map<String, String[]> map = request().body().asFormUrlEncoded();

        Note note = new Note();
        note.title = map.get("title")[0];
        note.content = map.get("content")[0];

        note.date = DateTime.parse(map.get("date")[0], DateTimeFormat.forPattern("dd-MM-yyyy")).toDate();
        note.save();

        return redirect(routes.Application.index());
    }
}
