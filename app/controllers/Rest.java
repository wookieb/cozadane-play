package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.Note;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;

import java.util.Date;

public class Rest extends Controller {

    public static Result getNote(String id) {
        Note note = NoteService.getNote(id);
        if (note != null) {
            return ok(Json.toJson(note));
        } else {
            return notFound();
        }
    }

    public static Result getAllNotes() {
        return ok(Json.toJson(NoteService.getNotes()));
    }

    public static Result addNote() {
        JsonNode jsonNode = request().body().asJson();
        Note note = Json.fromJson(jsonNode, Note.class);

        note.id = null;
        if (note.date == null) {
            note.date = new Date();
        }
        note.save();

        return ok(Json.toJson(note));
    }
}